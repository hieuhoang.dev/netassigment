﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPLC.Assignment3.DataLayer
{
    public abstract class Employee
    {
        //SSN, FirstName, LastName, BirthDate, Phone, Email
        public string SSN { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string BirthDate { get; set; }   
        public string Phone { get; set; }
        public string Email { get; set; }
        public Employee()
        {  
        }

        public Employee(string sSN, string firstName, string lastName, string birthDate, string phone, string email)
        {
            SSN = sSN;
            FirstName = firstName;
            LastName = lastName;
            BirthDate = birthDate;
            Phone = phone;
            Email = email;
        }
    }
}
