﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPLC.Assignment3.DataLayer
{
    public class HourlyEmployee : Employee
    {
        //Rate, WorkingHours
        public double Rate { get; set; }
        public double WorkingHours { get; set; }

        public HourlyEmployee()
        {
            
        }

        public HourlyEmployee(string sSN, string firstName, string lastName, string birthDate, string phone, string email, double rate, double workingHours) 
            : base(sSN, firstName, lastName, birthDate, phone, email)
        {
            Rate = rate;
            WorkingHours = workingHours;
        }
    }
}
