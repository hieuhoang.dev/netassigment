﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPLC.Assignment3.DataLayer
{
    public class SalariedEmployee : Employee
    {
        //CommisstionRate, GrossSales, BasicSalary
        public double CommisstionRate { get; set; }
        public double GrossSales { get; set; }
        public double BasicSalary { get; set; }

        public SalariedEmployee()
        {
            
        }

        public SalariedEmployee(string sSN, string firstName, string lastName, string birthDate, string phone, string email, double commisstionRate = 0, double grossSales = 0, double basicSalary = 0) : base(sSN, firstName, lastName, birthDate, phone, email)
        {
            CommisstionRate = commisstionRate;
            GrossSales = grossSales;
            BasicSalary = basicSalary;
        }
    }
}
