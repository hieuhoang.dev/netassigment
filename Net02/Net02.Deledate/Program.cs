﻿using System.Net.NetworkInformation;
using System.Text;
using static System.Net.Mime.MediaTypeNames;

namespace Net02.Deledate
{
    internal class Program
    {
        //Delegate là kiểu dữ liệu: Delegate là một kiểu dữ liệu giống như lớp hoặc cấu trúc(struct).
        //Nó định nghĩa kiểu của các phương thức mà delegate có thể tham chiếu tới.

        //Delegate là type-safe: Delegate đảm bảo rằng các phương thức được tham chiếu có chữ ký phù hợp,
        //giúp tránh lỗi thời gian chạy liên quan đến kiểu dữ liệu.

        //Delegate có thể đa điểm (multicast): Một delegate có thể tham chiếu đến nhiều phương thức cùng một lúc.Khi delegate được gọi,
        //tất cả các phương thức trong danh sách sẽ được gọi theo thứ tự.

        //MyDelegate: Khai báo một delegate tên là MyDelegate nhận một tham số kiểu string và trả về một giá trị kiểu int.
        delegate int MyDelegate(string s);
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;

            //convertToInt: Delegate trỏ tới phương thức ConvertStringToInt.
            //showString: Delegate trỏ tới phương thức ShowString.
            MyDelegate convertToInt = new MyDelegate(ConvertStringToInt);

            MyDelegate showString = new MyDelegate(ShowString);


            // multicast: Delegate kết hợp cả hai delegate convertToInt và showString.
            // Khi gọi multicast, cả hai phương thức sẽ được gọi theo thứ tự.

            MyDelegate multicast = convertToInt + showString;

            string numberSTR = "1309";

            Console.WriteLine("In kết quả: " + multicast(numberSTR));

            int valueConverted = convertToInt(numberSTR);

            Console.WriteLine("Giá trị đã convert thành int: " + valueConverted);

            Console.WriteLine("Kết quả khi gọi multicast Delegate");
            multicast(numberSTR);

            Console.ReadLine();
        }

        static int ConvertStringToInt(string stringValue)
        {
            int valueInt = 0;
            //ref (Reference)
            //Khởi tạo trước khi truyền: Biến được truyền vào phải được khởi tạo trước khi được truyền vào phương thức.
            //Sử dụng trong phương thức: Bên trong phương thức, biến được truyền bằng ref có thể được đọc và ghi giá trị.
            //Từ khóa trong khai báo và gọi: Từ khóa ref phải được sử dụng cả trong phần khai báo của phương thức và khi gọi phương thức.
            //out (Output)
            //Không cần khởi tạo trước khi truyền: Biến được truyền vào không cần phải khởi tạo trước khi được truyền vào phương thức.
            //Bắt buộc gán giá trị trong phương thức: Bên trong phương thức, biến được truyền bằng out phải được gán giá trị trước khi phương thức kết thúc.
            //Từ khóa trong khai báo và gọi: Từ khóa out phải được sử dụng cả trong phần khai báo của phương thức và khi gọi phương thức.
            Int32.TryParse(stringValue, out valueInt);
            Console.WriteLine("Đã ép kiểu dữ liệu thành công");

            return valueInt;
        }

        static int ShowString(string stringValue)
        {
            Console.WriteLine(stringValue);
            return 0;
        }
    }
}
