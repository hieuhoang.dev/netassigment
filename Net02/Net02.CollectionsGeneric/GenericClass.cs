﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net02.CollectionsGeneric
{
    internal class GenericClass<T> : IGenericInterface<T>
    {
        private T data;

        public GenericClass(T value)
        {
            data = value;
        }

        public T GetData()
        {
            return data;
        }
    }
}