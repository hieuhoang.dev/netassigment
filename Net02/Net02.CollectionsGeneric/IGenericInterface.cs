﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net02.CollectionsGeneric
{
    internal interface IGenericInterface<T>
    {
        T GetData();
    }
}
