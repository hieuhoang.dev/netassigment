﻿//Trong C#, "delegate" là một kiểu dữ liệu đặc biệt được sử dụng để tạo và tham chiếu đến các phương thức (hoặc phương thức mở rộng) trong C#. 
//Đây là một cơ chế quan trọng trong lập trình hướng sự kiện và lập trình đa luồng. Delegate cho phép bạn truyền phương thức như một tham số và gọi nó như một phương thức bình thường.

//Anonymous methods trong C# cho phép bạn khai báo và triển khai phương thức mà không cần đặt tên cho nó, thường được sử dụng khi bạn muốn xác định một phương thức ngắn gọn mà không cần phải tạo một phương thức riêng biệt. 
//Anonymous methods thường được sử dụng với delegate để cung cấp một phương thức ngắn gọn mà không cần tạo một phương thức tường minh.

//Lambda expressions là một cú pháp ngắn gọn trong C# (và các ngôn ngữ khác hỗ trợ) để tạo một hàm ẩn danh (anonymous function). 
//Lambda expression giúp triển khai ngắn gọn các phương thức delegate hoặc Expression Tree (cây biểu thức) một cách dễ đọc và súc tích.

//Trong C#, collections là các cấu trúc dữ liệu dùng để lưu trữ, quản lý và làm việc với tập hợp các phần tử dữ liệu. Có nhiều loại collection khác nhau trong C#, bao gồm mảng (arrays), danh sách (lists), tập hợp (sets), bản đồ (dictionaries), hàng đợi (queues), ngăn xếp (stacks), v.v. 
//Mỗi loại collection có cấu trúc và chức năng riêng biệt, phù hợp với nhu cầu cụ thể của ứng dụng.

//Generics trong C# cho phép bạn viết mã linh hoạt và tái sử dụng bằng cách cho phép bạn khai báo các lớp, phương thức và giao diện với kiểu dữ liệu được chỉ định sau khi mã được viết. Điều này mang lại nhiều lợi ích quan trọng:

//Tính linh hoạt và Tái sử dụng mã:
//Generics cho phép bạn viết mã một lần và sử dụng nó cho nhiều kiểu dữ liệu. Bạn có thể tạo lớp hoặc phương thức mà không cần cài đặt lại mã khi muốn áp dụng cho kiểu dữ liệu khác.

//Tính tổng quát và Linh hoạt:
//Generics giúp xây dựng các cấu trúc dữ liệu và thuật toán mà không cần biết trước kiểu dữ liệu sẽ được sử dụng. Điều này làm tăng sự linh hoạt và khả năng tái sử dụng mã.

//Tăng hiệu suất và hiệu quả:
//Generics giúp tránh việc ép kiểu (type casting) và phân chia mã thành các phương thức và lớp có thể hoạt động hiệu quả với một loạt các kiểu dữ liệu.

//Tính bảo đảm kiểu tại thời điểm biên dịch (Compile-Time Type Safety):
//Generics cung cấp kiểm tra kiểu dữ liệu tĩnh (static type checking) tại thời điểm biên dịch, giúp phát hiện lỗi kiểu dữ liệu trước khi chương trình được chạy.

//Giảm sự phụ thuộc vào Object:
//Trước khi Generics, thường phải làm việc với các đối tượng Object và phải thực hiện ép kiểu (type casting). Sử dụng Generics giúp tránh được việc này và mang lại hiệu suất tốt hơn.

//Iterator trong C# cho phép bạn duyệt qua một tập hợp dữ liệu một cách tuần tự mà không cần phải tải tất cả dữ liệu vào bộ nhớ trước. Điều này giữ cho bộ nhớ tối ưu và hiệu suất cao, đặc biệt khi làm việc với các tập dữ liệu lớn. Iterator được sử dụng phổ biến trong việc làm việc với các collection, arrays, và tập hợp dữ liệu.

//public T Delete(T data){}

using System.Collections;

namespace Net02.CollectionsGeneric
{
    public delegate string MyDelegate(string message);
    internal class Program
    {
        static string messStatic = "Net02";
        static string PrintMessage(string mess)
        {
            messStatic = "Net";
            Console.WriteLine("Showing: " + messStatic);
            //Console.WriteLine("Printing: " + message);
            return messStatic;
        }

        static string ShowMessage(string message)
        {
            Console.WriteLine("Showing: " + messStatic);
            messStatic = "Net";
            Console.WriteLine("Showing: " + messStatic);
            return messStatic;
        }


        public T GetData2<T>(T data)
        {
            return data;
        }

        static void Main(string[] args)
        {
            MyDelegate delegate1 = new MyDelegate(PrintMessage);

            //delegate1 = ShowMessage;
            //delegate1 += PrintMessage;
            MyDelegate delegate2 = new MyDelegate(ShowMessage);
            MyDelegate myDelegate = delegate1 + delegate2;

            //delegate1("Hello");
            //delegate2("World");
            Console.WriteLine(myDelegate("Test"));

            //MyDelegate delegate3 = delegate (string message)
            //{
            //    Console.WriteLine("Anonymous method: " + message);
            //};

            //delegate3("Hello");

            // Lambda expression để tính bình phương của một số
            Func<int, int> square = x => x * x;

            int result = square(5);
            Console.WriteLine("Bình phương của 5 là: " + result);

            // Khởi tạo danh sách (List) kiểu int
            List<object> numbers = new List<object>();

            object[] numbers2 = new object[5];
            numbers2[0] = true;
            numbers2[1] = 1;

            // Thêm phần tử vào danh sách
            numbers.Add(10);
            numbers.Add(20);
            numbers.Add(30);

            Console.WriteLine(numbers[0]);

            // Duyệt và in các phần tử trong danh sách
            Console.WriteLine("Các phần tử trong danh sách:");
            foreach (var num in numbers)
            {
                Console.WriteLine(num);
            }


            // Khởi tạo Dictionary kiểu string, int
            Dictionary<string, int> ages = new Dictionary<string, int>();

            // Thêm các cặp key-value vào Dictionary
            ages["Alice"] = 25;
            ages["Bob"] = 30;
            ages["Eve"] = 22;
            ages["Eve"] = 25;

            // Truy cập và in giá trị theo key
            Console.WriteLine("Tuổi của Bob: " + ages["Bob"]);

            // Duyệt và in các cặp key-value trong Dictionary
            Console.WriteLine("Các cặp key-value trong Dictionary:");
            foreach (var kvp in ages)
            {
                Console.WriteLine($"{kvp.Key}: {kvp.Value} tuổi");
            }

            // Khởi tạo Queue kiểu int
            Queue numbersQueue = new Queue();

            // Thêm phần tử vào hàng đợi
            numbersQueue.Enqueue(10);
            numbersQueue.Enqueue(20);
            numbersQueue.Enqueue(30);

            // Lấy và in phần tử từ hàng đợi
            Console.WriteLine("Phần tử đầu tiên: " + numbersQueue.Peek());

            // Lấy và in các phần tử từ hàng đợi theo thứ tự FIFO
            Console.WriteLine("Các phần tử trong hàng đợi:");
            while (numbersQueue.Count > 0)
            {
                Console.WriteLine(numbersQueue.Dequeue());
            }

            foreach (var item in numbersQueue)
            {
                Console.WriteLine(item);
            }

            // Khởi tạo ArrayList
            ArrayList arrayList = new ArrayList();

            // Thêm phần tử vào ArrayList
            arrayList.Add(10);
            arrayList.Add("Hello");
            arrayList.Add(3.14);

            // Duyệt và in các phần tử trong ArrayList
            Console.WriteLine("Các phần tử trong ArrayList:");
            foreach (var item in arrayList)
            {
                Console.WriteLine(item);
            }

            // Khởi tạo Hashtable
            Hashtable hashtable = new Hashtable();

            // Thêm các cặp key-value vào Hashtable
            hashtable["Alice"] = 25;
            hashtable["Bob"] = 30;
            hashtable["Eve"] = 22;
            hashtable["Eve"] = 25;
            hashtable[1] = 22;
            hashtable[""] = 25;
            hashtable[System.String.Empty] = 26;

            //hashtable[null] = 25;

            // Truy cập và in giá trị theo key
            Console.WriteLine("Tuổi của Bob: " + hashtable[""]);

            // Duyệt và in các cặp key-value trong Hashtable
            Console.WriteLine("Các cặp key-value trong Hashtable:");
            foreach (DictionaryEntry entry in hashtable)
            {
                Console.WriteLine($"{entry.Key}: {entry.Value} tuổi");
            }

            // Khởi tạo SortedList
            SortedList sortedList = new SortedList();

            // Thêm các cặp key-value vào SortedList
            sortedList.Add(4, 25);

            sortedList.Add(2, 22);
            sortedList.Add(1, 30);
            sortedList.Add(5, "9");

            //// Truy cập và in giá trị theo key
            //Console.WriteLine("Tuổi của Bob: " + sortedList["Bob"]);

            // Duyệt và in các cặp key-value trong SortedList
            Console.WriteLine("Các cặp key-value trong SortedList:");
            foreach (DictionaryEntry entry in sortedList)
            {
                Console.WriteLine($"{entry.Key}: {entry.Value} tuổi");
            }

            // Usage of the generic class and interface
            GenericClass<int> intGenericClass = new GenericClass<int>(42);
            Console.WriteLine("GenericClass<int> value: " + intGenericClass.GetData());

            GenericClass<string> stringGenericClass = new GenericClass<string>("Hello, world!");
            Console.WriteLine("GenericClass<string> value: " + stringGenericClass.GetData());

            // Usage of the non-generic class
            NonGenericClass nonGeneric = new NonGenericClass();
            nonGeneric.Display(123);
            nonGeneric.Display("Non-generic");

            int[] myNumbers = { 1, 2, 3, 4, 5 };
            MyCollection collection = new MyCollection(myNumbers);

            foreach (var number in collection)
            {
                Console.WriteLine(number);
            }

            string a = "OK";
            string b = "OK";
            string c = null;

            Console.WriteLine(a.Equals(b));
            Console.WriteLine(a.Equals(c));
            Console.WriteLine(b.Equals(c));

            Console.WriteLine(a == b);
            Console.WriteLine(a == c);
            Console.WriteLine(b == c);
        }
    }
}
