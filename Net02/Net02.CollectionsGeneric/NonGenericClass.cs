﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net02.CollectionsGeneric
{
    internal class NonGenericClass
    {
        public void Display<T>(T value)
        {
            Console.WriteLine("Displaying value: " + value);
        }
    }
}
