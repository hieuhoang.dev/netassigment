﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net02.CommonExpresion
{
    internal class ArgumentDemo
    {
        public void DisplayInfoNamedArguments(string name, int age)
        {
            Console.WriteLine("Name: " + name);
            Console.WriteLine("Age: " + age);
        }

        public void DisplayInfoOptionalArguments(string name, int age = 25, int year = 2023)
        {
            Console.WriteLine("Name: " + name);
            Console.WriteLine("Age: " + age);
        }

        public int Display(int a = 1)
        {
            int b = 0;
            if (a == 1)
            {
                b = a;
                return b;
            }
            return b;
        }
    }
}
