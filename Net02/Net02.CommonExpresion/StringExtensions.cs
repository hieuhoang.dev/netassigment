﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net02.CommonExpresion
{
    internal static class StringExtensions
    {
        //Extension methods(phương thức mở rộng) trong C# là một tính năng cho phép bạn thêm các phương thức mới vào một lớp (class) đã tồn tại mà không cần phải kế thừa hoặc sửa đổi mã nguồn của lớp đó. Điều này rất hữu ích khi bạn muốn mở rộng chức năng của một lớp mà không cần can thiệp vào mã nguồn gốc của lớp đó.

        //Để tạo extension method, bạn cần tuân thủ các bước sau:

        //Tạo một lớp tĩnh(static class) chứa phương thức mở rộng.
        //Tạo phương thức mở rộng bên trong lớp tĩnh đó.Phương thức này cần được đánh dấu bằng từ khóa this và phải xác định kiểu dữ liệu mà nó mở rộng.

        // Extension method để đảo ngược chuỗi
        public static string Reverse(this string input)
        {
            char[] chars = input.ToCharArray();
            Array.Reverse(chars);
            return new string(chars);
        }
    }
}
